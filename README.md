# Django base docker container   

* Copy django app to app/
* Sqlite DB should be in /app/var
* Change module=app_name.wsgi:application in env/config/app.ini
* Change MAINTAINER in env/docker/Dockerfile
* Change CONTAINER_NAME & HOST_SQLITE_PATH in build.sh
* Change HOST & PATH in deploy.sh