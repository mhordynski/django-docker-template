#!/bin/bash

docker build -f env/docker/Dockerfile ./ -t CONTAINER_NAME

docker rm -f CONTAINER_NAME;

docker run -d --name CONTAINER_NAME -v HOST_SQLITE_PATH:/app/var --restart=always -p 8000:80 CONTAINER_NAME